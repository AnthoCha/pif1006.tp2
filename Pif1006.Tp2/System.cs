﻿using System;
using System.Text;

namespace Pif1006.Tp2
{
    public class System
    {
        public Matrix2D A { get; private set; }
        public Matrix2D B { get; private set; }

        public System(Matrix2D a, Matrix2D b)
        {
            // Doit rester tel quel

            A = a;
            B = b;
        }

        public bool IsValid()
        {
            // À compléter (0.25 pt)
            // Doit vérifier si la matrix A est carrée et si B est une matrice avec le même nb
            // de ligne que A et une seule colonne, sinon cela retourne faux.
            // Avant d'agir avec le système, il faut toujours faire cette validation
            return A.IsSquare() && A.Matrix.GetLength(0) == B.Matrix.GetLength(0) && B.Matrix.GetLength(1) == 1;
        }

        public Matrix2D SolveUsingCramer()
        {
            // À compléter (1 pt)
            // Doit retourner une matrice X de même dimension que B avec les valeurs des inconnus 
            if (IsValid())
            {
                var determinant = A.Determinant();
                if (determinant != 0)
                {
                    var n = A.Matrix.GetLength(0);
                    var x = new Matrix2D("X", n, 1);

                    Matrix2D a;
                    for (var i = 0; i < n; i++)
                    {
                        a = new Matrix2D(string.Concat(A.Name, i), n, n);

                        for (var ai = 0; ai < n; ai++)
                        {
                            for (var j = 0; j < n; j++)
                            {
                                a.Matrix[ai, j] = j == i ? B.Matrix[ai, 0] : A.Matrix[ai, j];
                            }
                        }

                        x.Matrix[i, 0] = a.Determinant() / determinant;
                    }

                    return x;
                }
            }

            return Matrix2D.Null;
        }

        public Matrix2D SolveUsingInverseMatrix()
        {
            // À compléter (0.25 pt)
            // Doit retourner une matrice X de même dimension que B avec les valeurs des inconnus 

            if (IsValid())
            {
                var inv = A.Inverse();

                if (inv != null)
                {
                    return inv.Multiply(B);
                }
            }

            return Matrix2D.Null;
        }

        public Matrix2D SolveUsingGauss()
        {
            // À compléter (1 pts)
            // Doit retourner une matrice X de même dimension que B avec les valeurs des inconnus 
            if (IsValid())
            {
                var n = A.Matrix.GetLength(0);
                var a = (Matrix2D)A.Clone();
                var b = (Matrix2D)B.Clone();

                int i, j, iPivot;
                double c;

                for (iPivot = 0; iPivot < n; iPivot++)
                {
                    // Pivot inférieur = 0
                    for (i = n - 1; i > iPivot; i--)
                    {
                        c = (0 - a.Matrix[i, iPivot]) / a.Matrix[iPivot, iPivot];
                        b.Matrix[i, 0] = b.Matrix[i, 0] + b.Matrix[iPivot, 0] * c;
                        for (j = 0; j < n; j++)
                        {
                            a.Matrix[i, j] = a.Matrix[i, j] + a.Matrix[iPivot, j] * c;
                        }
                    }
                }

                for (iPivot = 0; iPivot < n; iPivot++)
                {
                    // Pivot supérieur = 0
                    for (i = 0; i < iPivot; i++)
                    {
                        c = (0 - a.Matrix[i, iPivot]) / a.Matrix[iPivot, iPivot];
                        b.Matrix[i, 0] = b.Matrix[i, 0] + b.Matrix[iPivot, 0] * c;
                        for (j = 0; j < n; j++)
                        {
                            a.Matrix[i, j] = a.Matrix[i, j] + a.Matrix[iPivot, j] * c;
                        }
                    }

                    // Pivot = 1
                    c = (1 - a.Matrix[iPivot, iPivot]) / a.Matrix[iPivot, iPivot];
                    b.Matrix[iPivot, 0] = b.Matrix[iPivot, 0] + b.Matrix[iPivot, 0] * c;
                    for (j = 0; j < n; j++)
                    {
                        a.Matrix[iPivot, j] = a.Matrix[iPivot, j] + a.Matrix[iPivot, j] * c;
                    }
                }

                return a.Multiply(b);
            }

            return Matrix2D.Null;
        }

        public override string ToString()
        {
            // À compléter (0.5 pt)
            // Devrait retourner en format:
            // 
            // 3x1 + 5x2 + 7x3 = 9
            // 6x1 + 2x2 + 5x3 = -1
            // 5x1 + 4x2 + 5x3 = 5
            var sb = new StringBuilder();

            if (IsValid())
            {
                var n = A.Matrix.GetLength(0);
                double val;

                for (var i = 0; i < n; i++)
                {
                    sb.AppendLine();
                    sb.AppendFormat("{0}x{1} ", A.Matrix[i, 0], 1);

                    for (var j = 1; j < n; j++)
                    {
                        val = A.Matrix[i, j];
                        sb.AppendFormat("{0} {1}x{2} ", val < 0 ? '-' : '+', Math.Abs(val), j + 1);
                    }

                    sb.AppendFormat("= {0}", B.Matrix[i, 0]);
                }
            }

            return sb.ToString();
        }
    }
}
