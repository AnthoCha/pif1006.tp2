﻿using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace Pif1006.Tp2
{
    // - Répartition des points -:
    // Program.cs: 2 pts
    // Matrix.cs: 4 pts
    // System.cs: 3 pts
    // Rapport + guide: 1 pt)

    class Program
    {
        static System system;

        static async Task Main(string[] args)
        {

            // À compléter: 2 pts (0.5 menu / 1.5 chargement)
            /* Vous devez avoir un menu utilisateur avec l'arboresence de menus suivants:
             * 1) Charger un fichier de système -> doit être un fichier structuré ou semi structurée qui décrit
             *    2 matrices; vous pouvez choisir de demander de charger 2 fichiers de matrices séparées (A et B)
             *    si vous préférez;
             *    
             *    ex. de format en "plain text" potentiel:
             *    
             *    3 1 5
             *    4 2 -1
             *    0 -6 4
             *    0
             *    4
             *    6
             *    
             *    Il faut ensuite "parser" ligne par ligne et déduire la taille de la matrice carrée (plusieurs
             *    façons de vérifier cela).  Créez le chargement dans une classe à part ou dans une méthode privée ici.
             *    Si le format est invalide, il faut retourner null ou l'équivalent et cela doit être
             *    indiqué à l'utilisateur; on affiche le système chargé (en appelant implicitement le TOString() du système);
             *    on retourne au menu dans tous les cas;
             *    
             *    Conseil: utilisez du JSON pour vous pratiquer
             *    
             *    Vous pouvez avoir un fichier chargé par défaut; je vous conseille d'avoir plusieurs fichiers de sy`stèmes sous la main prêt
             *    à être chargés.
             *    
             * 2) Afficher le système (note: et le ToString() du système en "mode équation", et les matrices en vue matrices qui composent les équiations
             * 3) Résoudre avec Cramer
             * 4) Résoudre avec la méthode de la matrice inverse : si dét. nul, on recoit nul et on doit afficher un message à l'utilisateur
             * 5) Résoudre avec Gauss
             * 
             * Après chaque option on revient au menu utilisateur, sauf pour quitter bien évidemment.
             * 
             */

            // Boucle principale pour le roulement de l'application
            while (true)
            {
                // Première intruction posée à l'utilisateur pour le chargement du fichier
                Console.Write("1) Veuillez entrer le nom du fichier JSON qui contient deux matrices : ");
                var path = Console.ReadLine();
                var loop = true;

                try
                {
                    // Chargement du fichier et désérialisation du JSON
                    using (var stream = File.OpenRead(path))
                    {
                        system = (await JsonSerializer.DeserializeAsync<JsonSystem>(stream)).ToSystem();
                    }

                }
                catch
                {
                    Console.WriteLine("Fichier invalide.");
                    loop = false;
                }
                // Boucle de menu 
                while (loop)
                {
                    Console.Write("Veuillez choisir une option parmi les suivantes : " +
                        "\n 2) Afficher le système" +
                        "\n 3) Résoudre avec Cramer" +
                        "\n 4) Résoudre avec la méthode de la matrice inverse" +
                        "\n 5) Résoudre avec Gauss" +
                        "\n 6) Changer de système" +
                        "\n 7) Quitter" +
                        "\n: ");

                    var entry = Console.ReadLine();
                    Matrix2D matrixX = null;
                    switch (entry)
                    {
                        case "2":
                            // Affichage du système 
                            Console.Write(system.ToString() + "\n \n");
                            break;
                        case "3":
                            // Résolution selon la méthode de Cramer
                            matrixX = system.SolveUsingCramer();
                            break;
                        case "4":
                            // Résolution selon la méthode de la matrice inverse
                            matrixX = system.SolveUsingInverseMatrix();
                            break;
                        case "5":
                            // Résolution selon la méthode de Gauss
                            matrixX = system.SolveUsingGauss();
                            break;
                        case "6":
                            // Fin de la boucle de menu, retour à la sélection du fichier
                            loop = false;
                            break;
                        case "7":
                            // Fin du programme
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Choix invalide.");
                            break;
                    }

                    if (matrixX != null)
                    {
                        string matrixOut = matrixX != Matrix2D.Null ? matrixX.ToString() : "[]";

                        Console.Write("\n" + matrixOut + "\n \n");
                    }
                }
            }
        }
    }
}