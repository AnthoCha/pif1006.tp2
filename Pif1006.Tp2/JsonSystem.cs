﻿using System.Linq;

namespace Pif1006.Tp2
{
    public class JsonSystem
    {
        public double[][] A { get; set; }
        public double[][] B { get; set; }

        public System ToSystem()
        {
            var a = new Matrix2D(nameof(A), A.Length, A.Max(line => line.Length));

            int i, j;
            for (i = 0; i < A.Length; i++)
            {
                for (j = 0; j < A[i].Length; j++)
                {
                    a.Matrix[i, j] = A[i][j];
                }
            }

            var b = new Matrix2D(nameof(B), B.Length, B.Max(line => line.Length));

            for (i = 0; i < B.Length; i++)
            {
                for (j = 0; j < B[i].Length; j++)
                {
                    b.Matrix[i, j] = B[i][j];
                }
            }

            return new System(a, b);
        }
    }
}
