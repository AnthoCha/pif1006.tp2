﻿using System;
using System.Text;

namespace Pif1006.Tp2
{
    public class Matrix2D : ICloneable
    {
        public double[,] Matrix { get; private set; }
        public string Name { get; private set; }

        public static readonly Matrix2D Null = new(null,0,0);

        public Matrix2D(string name, int lines, int columns)
        {
            // Doit rester tel quel

            Matrix = new double[lines, columns];
            Name = name;
        }

        public Matrix2D Transpose()
        {
            // À compléter (0.25 pt)
            // Doit retourner une matrice qui est la transposée de celle de l'objet
            var lines = Matrix.GetLength(0);
            var columns = Matrix.GetLength(1);
            var transpose = new Matrix2D(string.Format("({0})T", Name), columns, lines);

            for (var i = 0; i < lines; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    transpose.Matrix[j, i] = Matrix[i, j];
                }
            }

            return transpose;
        }

        public bool IsSquare()
        {
            // À compléter (0.25 pt)
            // Doit retourner vrai si la matrice est une matrice carrée, sinon faux
            return Matrix.Length > 0 && Matrix.GetLength(0) == Matrix.GetLength(1);
        }

        public double Determinant()
        {
            // À compléter (2 pts)
            // Aura sans doute des méthodes suppl. privée à ajouter,
            // notamment de nature récursive. La matrice doit être carrée de prime à bord.
            if (IsSquare())
            {
                var n = Matrix.GetLength(0);
                switch (n)
                {
                    case 1:
                        return Matrix[0, 0];
                    case 2:
                        return Matrix[0, 0] * Matrix[1, 1] - Matrix[0, 1] * Matrix[1, 0];
                    default:
                        var determinant = 0D;
                        for (var j = 0; j < n; j++)
                        {
                            determinant += Matrix[0, j] * Complement(0, j);
                        }
                        return determinant;
                }
            }

            return 0;
        }

        public Matrix2D Comatrix()
        {
            // À compléter (1 pt)
            // Doit retourner une matrice qui est la comatrice de celle de l'objet
            if (IsSquare())
            {
                var n = Matrix.GetLength(0);

                var comatrix = new Matrix2D(string.Format("com({0})", Name), n, n);

                for (var i = 0; i < n; i++)
                {
                    for (var j = 0; j < n; j++)
                    {
                        comatrix.Matrix[i, j] = Complement(i, j);
                    }
                }

                return comatrix;
            }

            return null;
        }

        public Matrix2D Inverse()
        {
            // À compléter (0.25 pt)
            // Doit retourner une matrice qui est l'inverse de celle de l'objet;
            // Si le déterminant est nul ou la matrice non carrée, on retourne null.
            if (IsSquare())
            {
                var determinant = Determinant();
                if (determinant != 0)
                {
                    return Comatrix().Transpose().Multiply(1 / determinant);
                }
            }

            return null;
        }

        public double Complement(int i, int j)
        {
            if (IsSquare())
            {
                return Math.Pow(-1, i + j) * Mineur(i, j).Determinant();
            }

            return 0;
        }

        public Matrix2D Mineur(int i, int j)
        {
            if (IsSquare())
            {
                var n = Matrix.GetLength(0);
                var mn = n - 1;

                var mineur = new Matrix2D(string.Format("M{0},{1}", i, j), mn, mn);

                var ni = 0;
                int nj;
                for (var mi = 0; mi < mn; mi++)
                {
                    if (ni == i)
                    {
                        ni++;
                    }

                    nj = 0;
                    for (var mj = 0; mj < mn; mj++)
                    {
                        if (nj == j)
                        {
                            nj++;
                        }

                        mineur.Matrix[mi, mj] = Matrix[ni, nj];

                        nj++;
                    }

                    ni++;
                }

                return mineur;
            }

            return null;
        }

        public Matrix2D Multiply(Matrix2D b)
        {
            var m = Matrix.GetLength(1);
            if (m == b.Matrix.GetLength(0))
            {
                var n = Matrix.GetLength(0);
                var p = b.Matrix.GetLength(1);
                var c = new Matrix2D(string.Format("({0} * {1})", Name, b.Name), n, p);

                for (var i = 0; i < n; i++)
                {
                    for (var j = 0; j < p; j++)
                    {
                        for (var k = 0; k < m; k++)
                        {
                            c.Matrix[i, j] += Matrix[i, k] * b.Matrix[k, j];
                        }
                    }
                }

                return c;
            }

            return null;
        }

        public Matrix2D Multiply(double scalar)
        {
            var lines = Matrix.GetLength(0);
            var columns = Matrix.GetLength(1);
            var matrix = new Matrix2D(string.Format("({0} * {1})", Name, scalar), lines, columns);

            for (var i = 0; i < lines; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    matrix.Matrix[i, j] = Matrix[i, j] * scalar;
                }
            }

            return matrix;
        }

        public override string ToString()
        {
            // À compléter (0.25 pt)
            // Doit retourner l'équivalent textuel/visuel d'une matrice.
            // P.ex.:
            // A:
            // | 3 5 7 |
            // | 6 2 5 |
            // | 5 4 5 |
            var sb = new StringBuilder();
            sb.AppendFormat("{0}:", Name);

            var lines = Matrix.GetLength(0);
            var columns = Matrix.GetLength(1);
            for (var i = 0; i < lines; i++)
            {
                sb.AppendLine();
                sb.Append("| ");

                for (var j = 0; j < columns; j++)
                {
                    sb.AppendFormat("{0} ", Matrix[i, j]);
                }

                sb.Append('|');
            }

            return sb.ToString();
        }

        public object Clone()
        {
            var lines = Matrix.GetLength(0);
            var columns = Matrix.GetLength(1);
            var matrix = new Matrix2D(Name, lines, columns);

            for (var i = 0; i < lines; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    matrix.Matrix[i, j] = Matrix[i, j];
                }
            }

            return matrix;
        }
    }
}
