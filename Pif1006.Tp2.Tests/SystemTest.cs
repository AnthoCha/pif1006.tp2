using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pif1006.Tp2.Framework;

namespace Pif1006.Tp2.Tests
{
    [TestClass]
    public class SystemTest
    {
        // Tests fail because of floating-point precision
        // TODO: Replace tests to expect whole numbers

        [TestMethod]
        public void SolveUsingCramer()
        {
            // Arrange
            var equalityComparer = new Array2dEqualityComparer<double>();

            var expected = new double[,]
            {
                { 35D / 18D },
                { 5D / 18D },
                { 11D / 18D }
            };

            var a = new Matrix2D("A", 3, 3);
            var b = new Matrix2D("B", 3, 1);

            a.Matrix[0, 0] = 2D;
            a.Matrix[0, 1] = 1D;
            a.Matrix[0, 2] = 3D;

            a.Matrix[1, 0] = 1D;
            a.Matrix[1, 1] = -2D;
            a.Matrix[1, 2] = 1D;

            a.Matrix[2, 0] = 1D;
            a.Matrix[2, 1] = 1D;
            a.Matrix[2, 2] = -2D;

            b.Matrix[0, 0] = 6D;
            b.Matrix[1, 0] = 2D;
            b.Matrix[2, 0] = 1D;

            var system = new System(a, b);

            // Act
            var actual = system.SolveUsingCramer().Matrix;

            // Assert
            Assert.IsTrue(equalityComparer.Equals(expected, actual));
        }

        [TestMethod]
        public void SolveUsingInverseMatrix()
        {
            // Arrange
            var equalityComparer = new Array2dEqualityComparer<double>();

            var expected = new double[,]
            {
                { 35D / 18D },
                { 5D / 18D },
                { 11D / 18D }
            };

            var a = new Matrix2D("A", 3, 3);
            var b = new Matrix2D("B", 3, 1);

            a.Matrix[0, 0] = 2D;
            a.Matrix[0, 1] = 1D;
            a.Matrix[0, 2] = 3D;

            a.Matrix[1, 0] = 1D;
            a.Matrix[1, 1] = -2D;
            a.Matrix[1, 2] = 1D;

            a.Matrix[2, 0] = 1D;
            a.Matrix[2, 1] = 1D;
            a.Matrix[2, 2] = -2D;

            b.Matrix[0, 0] = 6D;
            b.Matrix[1, 0] = 2D;
            b.Matrix[2, 0] = 1D;

            var system = new System(a, b);

            // Act
            var actual = system.SolveUsingInverseMatrix().Matrix;

            // Assert
            Assert.IsTrue(equalityComparer.Equals(expected, actual));
        }

        [TestMethod]
        public void SolveUsingGauss()
        {
            // Arrange
            var equalityComparer = new Array2dEqualityComparer<double>();

            var expected = new double[,]
            {
                { 35D / 18D },
                { 5D / 18D },
                { 11D / 18D }
            };

            var a = new Matrix2D("A", 3, 3);
            var b = new Matrix2D("B", 3, 1);

            a.Matrix[0, 0] = 2D;
            a.Matrix[0, 1] = 1D;
            a.Matrix[0, 2] = 3D;

            a.Matrix[1, 0] = 1D;
            a.Matrix[1, 1] = -2D;
            a.Matrix[1, 2] = 1D;

            a.Matrix[2, 0] = 1D;
            a.Matrix[2, 1] = 1D;
            a.Matrix[2, 2] = -2D;

            b.Matrix[0, 0] = 6D;
            b.Matrix[1, 0] = 2D;
            b.Matrix[2, 0] = 1D;

            var system = new System(a, b);

            // Act
            var actual = system.SolveUsingGauss().Matrix;

            // Assert
            Assert.IsTrue(equalityComparer.Equals(expected, actual));
        }
    }
}
