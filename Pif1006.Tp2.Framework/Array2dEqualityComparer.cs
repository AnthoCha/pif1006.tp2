﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Pif1006.Tp2.Framework
{
    public class Array2dEqualityComparer<T> : IEqualityComparer<T[,]>
    {
        public bool Equals(T[,] x, T[,] y)
        {
            var lines = x.GetLength(0);
            var columns = x.GetLength(1);

            if (lines == y.GetLength(0) && columns == y.GetLength(1))
            {
                for (var i = 0; i < lines; i++)
                {
                    for (var j = 0; j < columns; j++)
                    {
                        if (!x[i, j].Equals(y[i, j]))
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            return false;
        }

        public int GetHashCode([DisallowNull] T[,] obj)
        {
            return obj.GetHashCode();
        }
    }
}
